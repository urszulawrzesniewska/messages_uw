package pl.codementors.textmessages.model;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;
import pl.codementors.textmessages.Message;
import pl.codementors.textmessages.MessageManager;
import pl.codementors.textmessages.SimpleRepositoryAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

public class Tester {

    SimpleRepositoryAdapter simpleRepositoryAdapter;

    @Test
    public void MessageWhenTitleNotProvided() {
        Message message = new Message("Message title");

        final String messageTitle = message.getTitle();

        Assert.assertNull(message.getTitle());
        Assertions.assertThat(messageTitle).isNull();
    }

    @Test
    public void setTitle() {
        Message message = new Message("Message title");
        message.setTitle("this is a message subject");

        final String messageTitle = message.getTitle();

        Assertions.assertThat("this is a message subject")
                .as("name not set properly")
                .isEqualTo(messageTitle);
        Assert.assertEquals("name not set properly", "this is a message subject", message.getTitle());

    }
    @Test
    @PrepareForTest(MessageManager.class)
    public void addMessagesToFile() throws NullPointerException {
        Message message = new Message("Message title");

        File input = mock(File.class);
        when(input.exists()).thenReturn(true);
        when(input.isDirectory()).thenReturn(false);
        when(input.canRead()).thenReturn(true);

        FileReader fr = mock(FileReader.class);
        BufferedReader br = mock(BufferedReader.class);
        try {
            when(br.readLine()).thenReturn("Message title")
                    .thenReturn("Message author")
                    .thenReturn("Message content")
                    .thenReturn(null);
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            whenNew(FileReader.class).withArguments(input).thenReturn(fr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            whenNew(BufferedReader.class).withArguments(fr).thenReturn(br);
        } catch (Exception e) {
            e.printStackTrace();
        }

        simpleRepositoryAdapter.getall();

        Assertions.assertThat(simpleRepositoryAdapter.getall()).hasSize(0)
                .contains(new Message("Message title")
                        ,new Message("Message author")
                        ,new Message("Message content"));
    }

}
package pl.codementors.textmessages;

import java.util.List;

public interface MessageReader {

    List<Message> read (String message);
}

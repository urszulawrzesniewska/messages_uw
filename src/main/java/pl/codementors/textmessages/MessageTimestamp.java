package pl.codementors.textmessages;

import java.util.Date;

public class MessageTimestamp extends Date {
    int year;
    int month;
    int date;
    int hour;
    int minute;
    int second;

    public MessageTimestamp(long date) {
        super(date);
    }

    @Override
    public String toString() {
        return "MessageTimestamp{" +
                "year=" + year +
                ", month=" + month +
                ", date=" + date +
                ", hour=" + hour +
                ", minute=" + minute +
                ", second=" + second +
                '}';
    }
}

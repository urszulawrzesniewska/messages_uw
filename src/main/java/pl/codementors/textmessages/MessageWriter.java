package pl.codementors.textmessages;

import java.util.List;

public interface MessageWriter {

    List<Message> write (String message);

}

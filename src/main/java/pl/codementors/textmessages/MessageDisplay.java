package pl.codementors.textmessages;

public class MessageDisplay implements DisplayStrategy {


    public MessageDisplay(String title, String author, String content, long timestamp) {
        this.title = title;
        this.author = author;
        this.content = content;
        this.timestamp = timestamp;
    }

    private String title;
    private String author;
    private String content;
    private long timestamp;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String print(String message) {
        return "Message{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", content='" + content + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}

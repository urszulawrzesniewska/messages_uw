package pl.codementors.textmessages;

public interface DisplayStrategy {

    String print (String message);

}

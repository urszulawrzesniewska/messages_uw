package pl.codementors.textmessages;

import java.util.List;

    public class MessageService {

        private MessageRepository repository;

        public MessageService(MessageRepository repository) {
            this.repository = repository;
        }

        public boolean addMessageIfNotExists(Message message) {
            if (repository.getall().stream().anyMatch(message1 -> message== message1)) {
                return false;
            } else {
                repository.add(message);
                return true;
            }

        }
        public boolean readMessageIfExists(Message message) {
            if (repository.getall().stream().anyMatch(message1 -> message == message1)) {
                repository.read(message);
            } else {
            }
            return false;
        }
        public boolean writeMessageIfExists(Message message) {
            if (repository.getall().stream().anyMatch(message1 -> message == message1)) {
                repository.write(message);
            } else {
            }
            return true;
        }

        public boolean deleteByTitle(String messageTitle) {
            List<Message> messages = repository.findByTitle(messageTitle);
            for (Message message : messages) {
                repository.remove(message);
            }
            return messages.size() > 0;
        }

        public boolean deleteByAuthor(String messageAuthor) {
            List<Message> messages = repository.findByAuthor(messageAuthor);
            for (Message message : messages) {
                repository.remove(message);
            }
            return messages.size() > 0;
        }

        public boolean remove(Message message) {
            return repository.remove(message);
        }

        public List<Message> findWithTitle(String messageTitle) {
            return repository.findByTitle(messageTitle);
        }

        public List<Message> findWithAuthor(String messageAuthor) {
            return repository.findByAuthor(messageAuthor);
        }

        public List<Message> findAll() {
            return repository.getall();
        }
    }


package pl.codementors.textmessages;

import java.awt.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {
    final static SimpleRepository simpleRepository = new SimpleRepository(new Storage());
    final static MessageRepository messageRepository = new SimpleRepositoryAdapter(simpleRepository);
    final static MessageService messageService = new MessageService(messageRepository);
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        mainLoop:

        while (true) {
            System.out.println("Choose action: ");
            System.out.println("1. Add message");
            System.out.println("1a. Write message");
            System.out.println("2. Remove message");
            System.out.println("3. Show message list");
            System.out.println("4. Find message by its title");
            System.out.println("5. Find message by its author");
            System.out.println("6. Read message list");
            System.out.println("7. Filter message list");
            System.out.println("8. Print in short or long version");
            System.out.println("9. Quit");

            Scanner scanner = new Scanner(System.in);
            String choice = scanner.nextLine();

            if (choice.equals("1")) {
                System.out.println("Add message");
                Message selectedMessage = getSelectedMessage(scanner);
                messageService.addMessageIfNotExists(selectedMessage);
            }
            if (choice.equals("1a")) {
                System.out.println("Write added message");
                Message selectedMessage = getSelectedMessage(scanner);
                messageService.writeMessageIfExists(selectedMessage);
            }
            if (choice.equals("2")) {

                System.out.println("Remove message");
                Message message = getSelectedMessage(scanner);
                messageService.remove(message);

            }
            if (choice.equals("3")) {
                System.out.println("Showing message list");
                System.out.println(messageService.findAll());
            }
            if (choice.equals("4")) {
                System.out.println("Find message by title");
                System.out.println("Please enter message title");
                choice = scanner.nextLine();
                final List<Message> messages = messageService.findWithTitle(choice);
                messageService.findWithTitle(choice);
                System.out.println(messages);
            }
            if (choice.equals("5")) {
                System.out.println("Find message by author");
                System.out.println("Please enter message author");
                choice = scanner.nextLine();
                final List<Message> messages = messageService.findWithAuthor(choice);
                messageService.findWithAuthor(choice);
                System.out.println(messages);
            }
            if (choice.equals("6")) {
                System.out.println("Reading messages");
                Message selectedMessage = getSelectedMessage(scanner);
                messageService.readMessageIfExists(selectedMessage);
            }

            if (choice.equals("7")) {
                System.out.println("Filtering messages");
            }
            if (choice.equals("8")) {
              /*  System.out.println("Choose method: ");
                Message message = new message();
                if(choice.equals("Short")) {
                    MessageDisplay = new DisplayShort();
                } else if (choice.equals(("Long"))) {
                    MessageDisplay = new DisplayLong();
                }
                final Message message = new Message();
            }*/
                if (choice.equals("9")) {
                    break mainLoop;
                }
            }
        }
    }

        private static Message getSelectedMessage (Scanner scanner){
            String choice;
            MessageManager messageManager = new MessageManager();
            System.out.println("Provide message title");
            choice = scanner.nextLine();
            messageManager.withTitle(choice);
            System.out.println("Provide message author");
            choice = scanner.nextLine();
            messageManager.withAuthor(choice);
            System.out.println("Provide message content");
            choice = scanner.nextLine();
            messageManager.withContent(choice);
            System.out.println("Returning timestamp");
            System.out.println(new Date(System.currentTimeMillis()));
            return messageManager.build();
        }
    }

package pl.codementors.textmessages;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SimpleRepositoryAdapter implements MessageRepository {

    private SimpleRepository simpleRepository;

    public SimpleRepositoryAdapter(SimpleRepository simpleRepository) {
        this.simpleRepository = simpleRepository;
    }

    @Override
    public void add(Message message) {
        simpleRepository.add(message);
    }

    @Override
    public boolean remove(Message message) {
        return simpleRepository.remove(message);
        }

    @Override
    public void read(Message message) {
        simpleRepository.read(message);

    }

    @Override
    public void write(Message message) {
        simpleRepository.write(message);
    }

    @Override
    public List<Message> getall() {
        List<Message> messages = new ArrayList<>();
            messages.addAll(simpleRepository.find());
        return messages;
    }

    @Override
    public List<Message> findByTitle(String messageTitle) {
        return getall().stream()
                .filter(item -> item.getTitle()
                        .equals(messageTitle)).collect(Collectors.toList());    }

    @Override
    public List<Message> findByAuthor(String messageAuthor) {
        return getall().stream()
                .filter(item -> item.getTitle()
                        .equals(messageAuthor)).collect(Collectors.toList());    }
}


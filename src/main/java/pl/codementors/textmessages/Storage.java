package pl.codementors.textmessages;

import java.util.ArrayList;
import java.util.List;

public class Storage implements MessageStorage {

    private List<Message> messages = new ArrayList<>();

    @Override
    public void add(Message message) {
        if (messages != null) {
            messages.add(message);
        }
        else {
            throw new RuntimeException();
        }
    }

    @Override
    public boolean remove(Message message) {
        return messages.remove(message);
    }
    @Override
    public List<Message> getall() {
        List<Message> copyStorage = new ArrayList<>();
        copyStorage.addAll(messages);
        return copyStorage;
    }
}

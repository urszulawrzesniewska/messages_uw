package pl.codementors.textmessages;

import java.util.List;

public interface MessageRepository {

    void add(Message message);

    void read(Message message);

    void write(Message message);

    boolean remove(Message message);

    List<Message> getall();

    List<Message> findByTitle(String messageType);

    List<Message> findByAuthor (String messageAuthor);
}

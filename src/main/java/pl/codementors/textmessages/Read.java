package pl.codementors.textmessages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Read implements MessageReader{


    public List<Message> read(String message) {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {

            while (br.ready()) {
                String titleText = br.readLine();
                String authorText = br.readLine();
                String contentText = br.readLine();
                String timestampText = br.readLine();

                String name = String.valueOf(Integer.parseInt(titleText));
                String author = String.valueOf(Integer.parseInt(authorText));
                String content = String.valueOf(Integer.parseInt(contentText));
                long timestamp = Integer.parseInt(timestampText);
            }

        } catch (IOException ex) {

        }
        return null;
    }
}

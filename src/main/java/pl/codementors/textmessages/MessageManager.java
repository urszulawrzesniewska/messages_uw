package pl.codementors.textmessages;

public class MessageManager {

    private String title;
    private String author;
    private String content;
    private MessageTimestamp timestamp;


    public MessageManager withTitle(String title) {
        this.title = title;
        return this;
    }
    public MessageManager withContent(String content) {
        this.content = content;
        return this;
    }
    public MessageManager withAuthor(String type) {
        this.author = author;
        return this;
    }

    public MessageManager withTimestamp (MessageTimestamp timestamp) {
        this.timestamp = timestamp;
        return this;
    }
    public Message build() {
        Message p = new Message("Message title");
        p.setTitle(this.title);
        p.setAuthor(this.author);
        p.setContent(this.content);
        p.getTimestamp();
        return p;
    }

}
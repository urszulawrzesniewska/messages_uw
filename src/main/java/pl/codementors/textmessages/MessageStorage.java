package pl.codementors.textmessages;

import java.util.List;

public interface MessageStorage {

    void add (Message message);
    boolean remove (Message message);
    List<Message> getall();
}

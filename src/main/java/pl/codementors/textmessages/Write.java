package pl.codementors.textmessages;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;

public abstract class Write implements MessageWriter{

    public List<Message> write(BufferedReader br, BufferedWriter bw) {

        try {
            String line;
            while ((line = br.readLine()) != null) {
                bw.write((line));
                bw.newLine();
            }
        } catch (IOException ex) {

        }
        return null;
    }
}
